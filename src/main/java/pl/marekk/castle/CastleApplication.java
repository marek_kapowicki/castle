package pl.marekk.castle;

import pl.marekk.castle.application.game.Menu;

public class CastleApplication {
    public static void main(String[] args) {
        Menu.create().play();
    }
}

