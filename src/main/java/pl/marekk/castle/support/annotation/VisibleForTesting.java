package pl.marekk.castle.support.annotation;

/**
 *  Currently, this annotation has only informational purposes
 *  Informs about domain methods package visibility
 *  Those methods should be visible ONLY in package in test purposes
 */
public @interface VisibleForTesting {
}
