package pl.marekk.castle.support;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.exception.Exceptions;

import java.util.Scanner;

/**
 * another home made util
 */
public class ConsoleSupport {
    private static final Logger log = LogManager.getLogger(ConsoleSupport.class);

    private ConsoleSupport() {
    }


    public static void writeAndWait(String text){
        log.info(text);
        takeAny();
    }
    public static void takeAny() {
        log.info("press any letter...");
        take();
    }
    public static String take() {
       Scanner input = new Scanner(System.in);
       if(input.hasNext()) {
           return input.nextLine();
       }
       input.close();
       throw Exceptions.illegalState("some issue with console");

    }

    public static char takeChar() {
        Scanner input = new Scanner(System.in);
        if(input.hasNext()) {
            return input.next().charAt(0);
        }
        input.close();
        throw Exceptions.illegalState("some issue with console");

    }

}
