package pl.marekk.castle.support;

import pl.marekk.castle.application.exception.Exceptions;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import static java.util.Objects.requireNonNull;

/**
 * Apache commons provides a rather more brief version:
 * SerializationUtils.serialize(myObject)
 * there is a copy of this class
 */
public final class SerializationUtils {
    private SerializationUtils() {
    }

    public static byte[] serialize(Serializable obj) {
        requireNonNull(obj);
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(obj);
            return baos.toByteArray();
        } catch (Exception e) {
            throw Exceptions.serializable("serialization issue" + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T deserialize(InputStream inputStream) {
        requireNonNull(inputStream);
        try (ObjectInputStream in = new ObjectInputStream(inputStream)) {
            return (T) in.readObject();
        } catch (Exception e) {
            throw Exceptions.serializable("deserialization issue" + e.getMessage());
        }
    }
}
