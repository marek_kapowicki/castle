package pl.marekk.castle.support.random;

import java.util.function.Supplier;

public interface NumberGenerator extends Supplier<Integer> {
}
