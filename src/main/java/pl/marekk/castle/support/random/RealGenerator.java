package pl.marekk.castle.support.random;

import pl.marekk.castle.application.exception.GameIllegalStateException;
import pl.marekk.castle.support.IntClosedRange;

import java.util.Random;

/**
 * generates random int value between min and max (included both)
 */
public class RealGenerator {
    private final Random random = new Random();
    private IntClosedRange closedRange;

    public RealGenerator(IntClosedRange closedRange) {
        this.closedRange = closedRange;
    }

    public NumberGenerator generate() {
        return () -> random.ints(closedRange.getMin(), closedRange.getMax()+1)
                .findFirst()
                .orElseThrow(GameIllegalStateException::new);
    }
}
