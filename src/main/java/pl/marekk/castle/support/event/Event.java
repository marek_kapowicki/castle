package pl.marekk.castle.support.event;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Function represents the event
 * When event occurs then success function is invoked
 * In other case the failure function is invoked
 *
 * @param <T> type of input of the function
 * @param <R> the type of the result of the function
 */
public abstract class Event<T, R> implements Function<T, R> {
    private final Predicate<T> eventSuccessAnalyzer;
    private final Function<T, R> success;
    private final Function<T, R> failure;

    public Event(EventSuccessAnalyzer<T> eventSuccessAnalyzer, Function<T, R> success, Function<T, R> failure) {
        this.eventSuccessAnalyzer = eventSuccessAnalyzer;
        this.success = success;
        this.failure = failure;
    }

    @Override
    public R apply(T input) {
        if (eventSuccessAnalyzer.test(input)) {
            return success.apply(input);
        } else {
            return failure.apply(input);
        }
    }
}
