package pl.marekk.castle.support.event;

import java.util.function.Predicate;

@FunctionalInterface
public interface EventSuccessAnalyzer<T> extends Predicate<T> {
}
