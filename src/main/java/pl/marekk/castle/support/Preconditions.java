package pl.marekk.castle.support;

import pl.marekk.castle.application.exception.GameIllegalStateException;

/**
 * normally i use Preconditions from guava
 */
public final class Preconditions {

    private Preconditions() {
    }

    public static void checkArgument(boolean b) {
        checkArgument(b, "wrong argument");
    }

    public static void checkArgument(boolean b, String errorMessage) {
        if (!b) {
            throw new GameIllegalStateException(errorMessage);
        }
    }

}
