package pl.marekk.castle.support;

public class IntClosedRange {
    private final int min;
    private final int max;

    public IntClosedRange(int min, int max) {
        Preconditions.checkArgument(max >= min);
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
