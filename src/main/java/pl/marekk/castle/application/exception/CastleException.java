package pl.marekk.castle.application.exception;

/**
 * generic application exception
 */
public class CastleException extends RuntimeException {

    CastleException() {
        super();
    }

    CastleException(String message) {
        super(message);
    }
}
