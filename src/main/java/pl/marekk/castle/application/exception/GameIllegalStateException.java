package pl.marekk.castle.application.exception;

public class GameIllegalStateException extends CastleException {
    public GameIllegalStateException() {
        super();
    }

    public GameIllegalStateException(String message) {
        super(message);
    }
}
