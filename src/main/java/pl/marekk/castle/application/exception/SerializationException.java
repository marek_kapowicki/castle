package pl.marekk.castle.application.exception;

class SerializationException extends CastleException {
    SerializationException(String message) {
        super(message);
    }
}
