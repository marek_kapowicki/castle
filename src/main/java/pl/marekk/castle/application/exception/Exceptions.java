package pl.marekk.castle.application.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Exceptions {
    private static final Logger log = LogManager.getLogger(Exceptions.class);
    private Exceptions() {
    }
    public static CastleException serializable(String message) {
        log.error("serializable exception = {}", message);
        return new SerializationException(message);
    }

    public static CastleException notFound(String id) {
        log.error("object {} not found ", id);
        return new ObjectNotFoundException();
    }

    public static CastleException illegalState(String message) {
        log.error("game is now in illegal state ={}", message);
        return new GameIllegalStateException(message);
    }
}
