package pl.marekk.castle.application.input;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.command.CreateHero;
import pl.marekk.castle.support.ConsoleSupport;

import java.util.List;

public class Question {
    private static final Logger log = LogManager.getLogger(Question.class);
    private static final String QUESTION_ABOUT_PROFESSION = "Who are you %s?";
    private static final String QUESTION_ABOUT_NAME = "What is your name";
    private Question() {
    }

    public static CreateHero takeHeroFeatures(List<String> professions) {
        String name = readTheAnswer(Question.QUESTION_ABOUT_NAME);
        String profession = readTheAnswer(prepareQuestionAboutProfession(professions));
        return new CreateHero(name, profession);

    }

    private static String prepareQuestionAboutProfession(List<String> professions) {
        return String.format(Question.QUESTION_ABOUT_PROFESSION, professions);
    }

    private static String readTheAnswer(String question) {
        log.warn(question);
        return ConsoleSupport.take();
    }


}
