package pl.marekk.castle.application.input;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Intro {
    private static final Logger log = LogManager.getLogger(Intro.class);

    public static void printTitle() {
        log.info(TITLE);
    }
    public static void printBeforeStart() {

        log.info(BEGINING_PART_1);
        log.info(BEGINING_PART_2);

    }
    public static void printRest(String heroName) {
        log.info(String.format(BEGINING_PART_3, heroName));
        log.info(BEGINING_PART_4);
        log.info(BEGINING_PART_5);
    }


    private static final String TITLE =
         "\n _______  _______  _______  _       _________ _______    _______  _______  _______ _________ _        _______ ________ \n"+
                 "(  __  \\ (  ____ \\(       )(  ___  )( (    /|\\__   __/(  ____ \\  (  ____ \\(  ___  )(  ____ \\\\__   __/( \\      (  ____ \\ \n"+
                 "| (  \\  )| (    \\/| () () || (   ) ||  \\  ( |   ) (   | (    \\/  | (    \\/| (   ) || (    \\/   ) (   | (      | (    \\/\n"+
                 "| |   ) || (__    | || || || |   | ||   \\ | |   | |   | |        | |      | (___) || (_____    | |   | |      | (__    \n"+
                 "| |   | ||  __)   | |(_)| || |   | || (\\ \\) |   | |   | |        | |      |  ___  |(_____  )   | |   | |      |  __)   \n"+
                 "| |   ) || (      | |   | || |   | || | \\   |   | |   | |        | |      | (   ) |      ) |   | |   | |      | (      \n"+
                 "| (__/  )| (____/\\| )   ( || (___) || )  \\  |___) (___| (____/\\  | (____/\\| )   ( |/\\____) |   | |   | (____/\\| (____/\\\n"+
                 "(______/ (_______/|/     \\|(_______)|/    )_)\\_______/(_______/  (_______/|/     \\|\\_______)   )_(   (_______/(_______/\n";
    private static final String BEGINING_PART_1 =
            "In the land of Kielce a legend of old has been passed down from generation to generation. From\n" +
                    "within the Dark Forest there is an evil force plaguing the inhabitants of Blabla land. Many people\n" +
                    "have been struck down by sickness, suddenly dying where they stood or in their sleep. Many brave\n" +
                    "heroes have tried to uncover the truth behind this mystery, but to no avail. Many have come back,\n" +
                    "unrecognisable, as if a dark force was playing with their minds.\n";

    private static final String BEGINING_PART_2 =
            "You are a young poor peasant who has been dreaming about finding out what happened to the\n" +
                    "heroes of old who set out to discover the mystery behind these stories. Your elder brother set out\n" +
                    "to the city to barter but has been missing for some time and youíve decided to find out if he ís still\n" +
                    "alive.";
    private static final String BEGINING_PART_3 = "you ready the for the journey of your life young %s";

    private static final String BEGINING_PART_4 =
            "The hour is young, at dawn you open your eyes and prepare to set out through the forest following\n" +
            "the tracks of your elder brother. A loaf of bread is all you take with yourself.\n" +
            "Between your village and the city of Blabla there is a forest from which you start your journey.\n" +
            "While walking through the forest you hear whispers calling out ...help me .\n" +
            "It sounds like your elder brother, and you are not sure if you should follow them. The urge of\n" +
            "discovering what happened to your elder brother is growing stronger and you go deeper into the\n" +
            "forest.";
    private static final String BEGINING_PART_5 = "Suddenly you find yourself staring at an old castle.\n" +
            "The main gate creeks open. You check your inventory and prepare yourself.\n";

    private Intro() {
    }
}
