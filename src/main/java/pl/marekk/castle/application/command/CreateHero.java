package pl.marekk.castle.application.command;

import pl.marekk.castle.support.annotation.cqrs.Command;

@Command
public class CreateHero {
    private final String name;
    private final char professionChar;

    public CreateHero(String name, String profession) {
        this.name = name;
        this.professionChar = profession != null && !profession.isEmpty() ? profession.toUpperCase().charAt(0) : 'u';
    }

    public String getName() {
        return name;
    }

    public char getProfessionChar() {
        return professionChar;
    }

    @Override
    public String toString() {
        return "CreateHeroCommand{" +
                "name='" + name + '\'' +
                ", profession='" + professionChar + '\'' +
                '}';
    }
}
