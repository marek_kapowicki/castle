package pl.marekk.castle.application.command;

import pl.marekk.castle.domain.location.item.Item;
import pl.marekk.castle.support.annotation.cqrs.Command;

import java.util.Objects;
import java.util.UUID;

@Command
public class TakeItem {
    private final UUID id;
    private final Item item;

    public TakeItem(UUID id, Item item) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(item);
        this.id = id;
        this.item = item;
    }

    public UUID getId() {
        return id;
    }

    public Item getItem() {
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TakeItem takeItem = (TakeItem) o;
        return Objects.equals(id, takeItem.id) &&
                Objects.equals(item, takeItem.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, item);
    }

    @Override
    public String toString() {
        return "TakeItem{" +
                "id=" + id +
                ", item=" + item +
                '}';
    }
}
