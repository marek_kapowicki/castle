package pl.marekk.castle.application.command;

import pl.marekk.castle.support.annotation.cqrs.Command;

import java.util.Objects;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

@Command
public class SaveHero {
    private final UUID id;
    private final String path;

    public SaveHero(UUID id, String path) {
        requireNonNull(id);
        requireNonNull(path);
        this.id = id;
        this.path = path;
    }

    public UUID getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SaveHero saveHero = (SaveHero) o;
        return Objects.equals(id, saveHero.id) &&
                Objects.equals(path, saveHero.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, path);
    }

    @Override
    public String toString() {
        return "SaveHero{" +
                "id=" + id +
                ", path='" + path + '\'' +
                '}';
    }
}
