package pl.marekk.castle.application.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.command.CreateHero;
import pl.marekk.castle.application.input.Intro;
import pl.marekk.castle.support.ConsoleSupport;

import static pl.marekk.castle.application.input.Question.takeHeroFeatures;

class GameFlow {
    private static final Logger log = LogManager.getLogger(GameFlow.class);
    private Game game;
    private GameCommandDispatcher commandDispatcher;

    GameFlow() {
        Intro.printTitle();
        this.game = new Game();
    }

    void start() {
        Intro.printBeforeStart();
        CreateHero createHero = takeHeroFeatures(Game.retrieveProfessions());
        game.init(createHero);
        Intro.printRest(createHero.getName());
        commandDispatcher = new GameCommandDispatcher(game);
    }

    void play() {
        game.help();
        while (game.isFinished()) {
            char command = ConsoleSupport.takeChar();
            commandDispatcher.dispatch(command);
        }
        log.warn("the game is over!!");
    }

    void load() {
        game.load();
        commandDispatcher = new GameCommandDispatcher(game);
    }
    void exit() {
        Game.exit();
    }
}
