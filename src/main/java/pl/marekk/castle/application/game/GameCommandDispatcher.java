package pl.marekk.castle.application.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static pl.marekk.castle.application.exception.Exceptions.illegalState;

class GameCommandDispatcher {
    private static final Logger log = LogManager.getLogger(GameCommandDispatcher.class);
    private static final List<GameCommand> commands = asList(GameCommand.values());
    private final Game game;

    GameCommandDispatcher(Game game) {
        this.game = game;
    }

    static List<String> getCommands() {
        return commands.stream()
                .map(GameCommand::toString)
                .collect(toList());
    }

    void dispatch(char key) {
        switch (GameCommand.from(key)) {
            case QUIT:
                Game.exit();
                break;
            case SAVE:
                game.save();
                break;
            case LOAD:
                game.load();
                break;
            case EXPLORE:
                game.explore();
                break;
            case HELP:
                game.help();
                break;
            case INFO:
                game.status();
                break;
            default:
                log.error("unknow command");
        }
    }

    private enum GameCommand {
        QUIT('q'), SAVE('s'), LOAD('l'), EXPLORE('e'), HELP('h'), INFO('i');

        private char value;

        GameCommand(char value) {
            this.value = value;
        }

        static GameCommand from(char key) {
            return Arrays.stream(GameCommand.values())
                    .filter(it -> key == it.getValue())
                    .findFirst()
                    .orElseThrow(()-> illegalState("command not found"));
        }

        public char getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value + "-" + name();
        }


    }
}
