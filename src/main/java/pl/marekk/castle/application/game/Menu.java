package pl.marekk.castle.application.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.exception.CastleException;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static pl.marekk.castle.application.exception.Exceptions.illegalState;
import static pl.marekk.castle.support.ConsoleSupport.takeChar;

public class Menu {
    private static final Logger log = LogManager.getLogger(Menu.class);
    private static final List<MenuCommand> commands = asList(MenuCommand.values());
    private final GameFlow gameFlow;

    private Menu(GameFlow gameFlow) {
        this.gameFlow = gameFlow;
    }
    public static Menu create() {
        log.warn(Menu.getCommands());
        return new Menu(new GameFlow());
    }

    private static List<String> getCommands() {
        return commands.stream()
                .map(MenuCommand::toString)
                .collect(toList());
    }

    public void play() {
        try {
            char key = takeChar();
            play(key);
        } catch (CastleException e) {
            log.error("game is over");
        }
    }

    private void play(char key) {
        switch (MenuCommand.from(key)) {
            case START:
                gameFlow.start();
                gameFlow.play();
                break;
            case LOAD:
                gameFlow.load();
                gameFlow.play();
                break;
            case QUIT:
                gameFlow.exit();
                break;
            default:
                log.error("unknow command");
        }
    }

    private enum MenuCommand {
        START('s'), LOAD('l'), QUIT('q');

        private char value;

        MenuCommand(char value) {
            this.value = value;
        }

        static MenuCommand from(char key) {
            return Arrays.stream(MenuCommand.values())
                    .filter(it -> key == it.getValue())
                    .findFirst()
                    .orElseThrow(()-> illegalState("command not found"));
        }

        public char getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value+"-"+name();
        }


    }
}
