package pl.marekk.castle.application.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.command.CreateHero;
import pl.marekk.castle.application.command.SaveHero;
import pl.marekk.castle.application.command.TakeItem;
import pl.marekk.castle.domain.character.HeroGateway;
import pl.marekk.castle.domain.character.HeroRepresentation;
import pl.marekk.castle.domain.location.Location;
import pl.marekk.castle.domain.location.LocationGateway;
import pl.marekk.castle.support.annotation.VisibleForTesting;

import java.util.List;
import java.util.UUID;

class Game {
    private static final Logger log = LogManager.getLogger(Game.class);
    private static final String PATH_TO_SAVE = "saves";
    private final HeroGateway heroGateway;
    private final LocationGateway locationGateway;
    private UUID heroId;


    Game() {
        this.heroGateway = new HeroGateway();
        locationGateway = new LocationGateway();
    }

    @VisibleForTesting
    Game(HeroGateway heroGateway, LocationGateway locationGateway) {
        this.heroGateway = heroGateway;
        this.locationGateway = locationGateway;
    }

    UUID init(CreateHero command) {
        heroId = createHero(command);
        return heroId;
    }

    static List<String> retrieveProfessions() {
        return new HeroGateway().retrieveProfessions();
    }

    boolean isFinished() {
        HeroRepresentation info = heroGateway.getInfo(heroId);
        return info.isAlive();
    }

    static void exit() {
        log.warn("the game is over");
        System.exit(0);
    }
    private UUID createHero(CreateHero command) {
        return heroGateway.createHero(command);
    }

    void save() {
        heroGateway.save(new SaveHero(heroId, PATH_TO_SAVE));
    }

    void load() {
        heroId = heroGateway.load(PATH_TO_SAVE);
        status();
    }

    void explore() {
        Location location = locationGateway.create();
        log.info("explore the next location {}", location);
        heroGateway.take(new TakeItem(heroId, location.getExhibit()));
    }

    void help() {
        log.info("available commands");
        log.warn(GameCommandDispatcher.getCommands());
    }

    HeroRepresentation status() {
        HeroRepresentation hero = heroGateway.getInfo(heroId);
        log.warn("your status: {}", hero.getStatus());
        return hero;
    }
}
