package pl.marekk.castle.domain;

import pl.marekk.castle.application.exception.Exceptions;
import pl.marekk.castle.domain.character.Hero;
import pl.marekk.castle.support.SerializationUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static pl.marekk.castle.support.SerializationUtils.serialize;

public class HeroSaver {
    public void save(Hero hero, String path) {
        byte[] bytes = serialize(hero);
        try {
            Files.write(Paths.get(path), bytes);
        } catch (IOException e) {
            throw Exceptions.illegalState("exception during save");
        }
    }
    public Hero load(String path) {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            return SerializationUtils.deserialize(new ByteArrayInputStream(bytes));
        } catch (IOException e) {
            throw Exceptions.illegalState("exception during load");
        }
    }
}
