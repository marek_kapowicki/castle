package pl.marekk.castle.domain.character.fight;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.domain.character.Character;
import pl.marekk.castle.support.Pair;
import pl.marekk.castle.support.annotation.VisibleForTesting;

/**
 * represents the battle between two characters
 */
public class Battle {
    private static  final Logger log = LogManager.getLogger(Battle.class);
    private final Character first;
    private final Character second;
    private final AttackEvent attackEvent;

    @VisibleForTesting
    Battle(Character first, Character second, AttackEvent attackEvent) {
        this.first = first;
        this.second = second;
        this.attackEvent = attackEvent;
    }

    public static Battle of(final Character first, final Character second) {
        return new Battle(first, second, AttackEvent.of());
    }

    /**
     * fight between 2 characters till the end (ine of attenders will die)
     *
     * @return pair of characters after battle
     */
    public Pair<Character, Character> fight() {
        Character firstFighter = first;
        Character secondFighter = second;
        log.info("battle  between {} and {} has just started", firstFighter.getStatus(), secondFighter.getStatus());
        while (firstFighter.isAlive() && secondFighter.isAlive()) {
            secondFighter = attack(firstFighter, secondFighter);

            if (!secondFighter.isAlive()) {
                break;
            }

            firstFighter = attack(secondFighter, firstFighter);

        }
        return new Pair<>(firstFighter, secondFighter);
    }

    private Character attack(Character attacker, Character defender) {
        return attackEvent.apply(new Pair<>(defender, attacker.getAttack()));
    }
}
