package pl.marekk.castle.domain.character.fight;

import pl.marekk.castle.domain.character.Character;
import pl.marekk.castle.support.IntClosedRange;
import pl.marekk.castle.support.Pair;
import pl.marekk.castle.support.event.EventSuccessAnalyzer;
import pl.marekk.castle.support.random.RealGenerator;

class HitAnalyzer implements EventSuccessAnalyzer<Pair<Character, Integer>> {
    private final RealGenerator generator;

    private HitAnalyzer(RealGenerator generator) {
        this.generator = generator;
    }

    static HitAnalyzer realAnalyzer() {
        return new HitAnalyzer(new RealGenerator(new IntClosedRange(0, 10)));
    }

    public boolean test(Pair<Character, Integer> pair) {
        return generator.generate().get() >= pair.getLeft().getSpeed();
    }

}
