package pl.marekk.castle.domain.character;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.domain.character.fight.Battle;
import pl.marekk.castle.domain.location.item.Item;
import pl.marekk.castle.support.Pair;
import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.annotation.ddd.AggregateRoot;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import static java.util.Objects.requireNonNull;
import static java.util.UUID.randomUUID;

@AggregateRoot
public class Hero implements Character, Serializable {
    private static final Logger log = LogManager.getLogger(Hero.class);
    private final UUID id;
    private final String name;
    private final Profession profession;
    private final int lifePoints;
    private final int speed;
    private final int power;
    @VisibleForTesting
    final int experience;
    @VisibleForTesting
    final Item weapon;

    Hero(String name, Profession profession, int life) {
        this(randomUUID(), name, profession, life, profession.getSpeed(), profession.getStrength(), 0, null);
    }

    private Hero(UUID id, String name, Profession profession, int life, int speed, int power, int experience, Item weapon) {
        this.id = id;
        this.name = name;
        this.profession = profession;
        this.lifePoints = life;
        this.speed = speed;
        this.power = power;
        this.experience = experience;
        this.weapon = weapon;
    }

    @Override
    public int getAttack() {
        return power + getPowerFromItem(weapon);
    }

    @Override
    public int getDefence() {
        return lifePoints;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public Hero withLifeAdded(int lifeToAdd) {
        int newLifePoints = calculateLife(lifeToAdd);
        return this.lifePoints == newLifePoints ? this : new Hero(id, name, profession, newLifePoints, speed, power, experience, weapon);
    }

    Hero takeItem(Item item) {
        requireNonNull(item);
        log.warn("You have just found {}", item);
        return item.applyFor(this);
    }

    public Hero fightWith(Character character, int experienceToGain) {
        return fightWith(character, experienceToGain, Battle.of(this, character));
    }
    @VisibleForTesting
    Hero fightWith(Character character, int experienceToGain, Battle battle) {
        Pair<Character, Character> result = battle.fight();
        Hero heroResult = (Hero) result.getLeft();
        if(heroResult.isAlive()){
            log.warn("Congrats!!! you win the battle");
            return heroResult.withExperienceAdded(experienceToGain);
        }
        return heroResult;
    }

    private Hero withExperienceAdded(int experienceToAdd) {
        return new Hero(id, name, profession, lifePoints, speed, power, experience + experienceToAdd, weapon);
    }

    public Hero withWeapon(Item weapon) {
        requireNonNull(weapon);
        return weapon.equals(this.weapon) ? this : new Hero(id, name, profession, lifePoints, speed, power, experience, weapon);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Hero hero = (Hero) o;
        return Objects.equals(id, hero.id);
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", profession=" + profession.name() +
                ", lifePoints=" + lifePoints +
                ", speed=" + speed +
                ", power=" + power +
                ", attack=" + getAttack()+
                ", experience=" + experience +
                weaponToString() +
                '}';
    }

    private String weaponToString() {
        return this.weapon == null? "" :  ", weapon=" + weapon.name();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    private int getPowerFromItem(Item item) {
        if (item == null) {
            return 0;
        }
        return item.getPower();
    }

}