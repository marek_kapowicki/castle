package pl.marekk.castle.domain.character;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.command.CreateHero;
import pl.marekk.castle.application.command.SaveHero;
import pl.marekk.castle.application.command.TakeItem;
import pl.marekk.castle.application.exception.Exceptions;
import pl.marekk.castle.domain.HeroSaver;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static pl.marekk.castle.domain.character.HeroFactory.heroFactory;

/**
 * gateway for domain object hero
 * the only one entrance to hero domain
 */
public class HeroGateway {
    private static final Logger log = LogManager.getLogger(HeroGateway.class);

    private final HeroRepository repository;
    private final HeroFactory heroFactory;
    private final HeroSaver heroSaver;
    private final Map<java.lang.Character, Profession> professionsCache;

    public HeroGateway() {
        this.repository = HeroRepository.of();
        this.heroFactory = heroFactory();
        this.heroSaver = new HeroSaver();
        this.professionsCache = storeProfessionsInCache();
    }


    public List<String> retrieveProfessions() {
        log.debug("create cache");
        return professionsCache.values()
                .stream()
                .map(Profession::toString)
                .collect(toList());
    }

    public UUID createHero(CreateHero command) {
        validateCommand(command);
        log.warn("creating the new hero from command={}", command);
        Hero created = heroFactory.create(command.getName(), professionsCache.get(command.getProfessionChar()));
        repository.saveOrUpdate(created.getId(), created);
        return created.getId();
    }

    public HeroRepresentation getInfo(UUID uuid) {
        log.debug("retrieving status of hero");
        Hero retrieved = repository.retrieve(uuid);
        return new HeroRepresentation(retrieved.toString(), retrieved.getDefence(), retrieved.experience);
    }
    public void save(SaveHero saveHero) {
        requireNonNull(saveHero);
        Hero retrieved = repository.retrieve(saveHero.getId());
        log.warn("saving the {} to file", retrieved, saveHero.getPath());
        heroSaver.save(retrieved, saveHero.getPath());
    }

    public UUID load(String path) {
        log.warn("loading the hero from file {}", path);
        Hero loaded = heroSaver.load(path);
        repository.saveOrUpdate(loaded.getId(), loaded);
        return loaded.getId();
    }

    public UUID take(TakeItem takeItem) {
        requireNonNull(takeItem);
        Hero updatedHero = repository.retrieve(takeItem.getId()).takeItem(takeItem.getItem());
        if (!updatedHero.isAlive()) {
            log.error("You Died :(");
        }
        repository.saveOrUpdate(takeItem.getId(), updatedHero);
        return takeItem.getId();
    }
    private void validateCommand(CreateHero command) {
        requireNonNull(command);
        requireNonNull(command.getName());
        requireNonNull(command.getProfessionChar());
        if(!professionsCache.containsKey(command.getProfessionChar())) {
            throw Exceptions.notFound(String.valueOf(command.getProfessionChar()));
        }
    }

    private Map<java.lang.Character, Profession> storeProfessionsInCache() {
        return Arrays
                .stream(Profession.values())
                .collect(Collectors.toMap(it -> it.name().toUpperCase().charAt(0), Function.identity()));
    }


}
