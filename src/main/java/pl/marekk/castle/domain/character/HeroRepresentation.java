package pl.marekk.castle.domain.character;

import pl.marekk.castle.support.annotation.ddd.ValueObject;

import java.util.Objects;

@ValueObject
//the simplest representation of hero
public class HeroRepresentation {
    private final String status;
    private final int life;
    private final int experience;

    HeroRepresentation(String status, int life, int experience) {
        this.status = status;
        this.life = life;
        this.experience = experience;
    }

    public String getStatus() {
        return status;
    }

    public int getExperience() {
        return experience;
    }
    public boolean isAlive() {
        return life > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeroRepresentation that = (HeroRepresentation) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
