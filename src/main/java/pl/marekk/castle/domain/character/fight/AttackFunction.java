package pl.marekk.castle.domain.character.fight;

import pl.marekk.castle.domain.character.Character;
import pl.marekk.castle.support.Pair;

import java.util.function.Function;

/**
 * given character is attacked with defined power
 */
interface AttackFunction extends Function<Pair<Character, Integer>, Character> {
}
