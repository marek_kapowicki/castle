package pl.marekk.castle.domain.character.fight;

import pl.marekk.castle.domain.character.Character;
import pl.marekk.castle.support.Pair;
import pl.marekk.castle.support.event.Event;
import pl.marekk.castle.support.event.EventSuccessAnalyzer;

/**
 * during the attack the random number is drawn
 * if it greater or equal then defender speed then attack is success
 * in other case defender avoids the hit
 */
class AttackEvent extends Event<Pair<Character, Integer>, Character> {
    static final AttackFunction miss = Pair::getLeft;
    static final AttackFunction hit = p -> p.getLeft().withLifeAdded(-p.getRight());

    private AttackEvent(EventSuccessAnalyzer<Pair<Character, Integer>> eventSuccessAnalyzer, AttackFunction success, AttackFunction failure) {
        super(eventSuccessAnalyzer, success, failure);
    }

    static AttackEvent of() {
        return of(HitAnalyzer.realAnalyzer());
    }

    static AttackEvent of(EventSuccessAnalyzer<Pair<Character, Integer>> analyzer) {
        return new AttackEvent(analyzer, hit, miss);
    }

}
