package pl.marekk.castle.domain.character;

import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.annotation.ddd.DomainFactory;

@DomainFactory
public class HeroFactory {
    private static final int DEFAULT_LIFE = 100;

    private final int initLife;

    private HeroFactory() {
        this(DEFAULT_LIFE);
    }

    @VisibleForTesting
    HeroFactory(int initLife) {
        this.initLife = initLife;
    }

    public static HeroFactory heroFactory() {
        return new HeroFactory();
    }
    public Hero create(String name, Profession profession) {
        return new Hero(name, profession, initLife);
    }
}
