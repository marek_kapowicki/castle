package pl.marekk.castle.domain.character;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.marekk.castle.application.exception.Exceptions;
import pl.marekk.castle.support.annotation.ddd.DomainRepository;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;


/**
 * map imitates the storage
 */
@DomainRepository
class HeroRepository {
    private static final Logger log = LogManager.getLogger(HeroRepository.class);
    private Map<UUID, Hero> storage = Collections.emptyMap();

    private HeroRepository() {
    }

    static HeroRepository of() {
        return new HeroRepository();
    }

    Hero retrieve(UUID id) {
        requireNonNull(id);
        log.debug("retrieving by id={}", id);
        Hero retrieved = storage.get(id);
        if(retrieved == null) {
            throw Exceptions.notFound(id.toString());
        }
        return retrieved;
    }

    Hero saveOrUpdate(UUID id, Hero newValue) {
        requireNonNull(id);
        requireNonNull(newValue);
        log.debug("updating hero identified by id={}", id);
        storage = singletonMap(id, newValue);
        return newValue;
    }

}
