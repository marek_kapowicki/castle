package pl.marekk.castle.domain.character;

public interface Character{
    int getAttack();
    int getDefence();
    int getSpeed();
    Character withLifeAdded(int life);

    default int calculateLife(int lifeToAdd) {
        return Math.max(0, getDefence() + lifeToAdd);
    }

    default boolean isAlive() {
        return getDefence() > 0;
    }
    default String getStatus() {
        return Status.generate(this);
    }
}
