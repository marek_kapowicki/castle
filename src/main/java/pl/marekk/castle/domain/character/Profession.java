package pl.marekk.castle.domain.character;

enum Profession {
    THIEF(7, 3, "(Fast but weak)"), BLACKSMITH(3, 7, "(Strong but slow)");

    private final int speed;
    private final int strength;
    private final String description;

    Profession(int speed, int strength, String description) {
        this.speed = speed;
        this.strength = strength;
        this.description = description;
    }

    @Override
    public String toString() {
        return name()+" "+description;
    }

    public int getSpeed() {
        return speed;
    }

    public int getStrength() {
        return strength;
    }
}
