package pl.marekk.castle.domain.character;

import java.util.StringJoiner;

final class Status {
    private Status() {
    }

    static String generate(Character character) {
        StringJoiner joiner = new StringJoiner(", ", character.getClass().getSimpleName() + "(", ")");

        joiner.add(addFeature("attack", character.getAttack()));
        joiner.add(addFeature("speed", character.getSpeed()));
        joiner.add(addFeature("defence", character.getDefence()));

        return joiner.toString();
    }

    private static String addFeature(String name, int value) {
        return addFeature(name, Integer.toString(value));
    }

    private static String addFeature(String name, String value) {
        return String.join("=", name, value);
    }
}

