package pl.marekk.castle.domain.location;

import pl.marekk.castle.domain.location.item.Item;

import java.util.Objects;

public class Chamber implements Location {
    private final Item exhibit;
    private final String description;

    Chamber(Item exhibit, String description) {
        this.exhibit = exhibit;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chamber chamber = (Chamber) o;
        return Objects.equals(exhibit, chamber.exhibit) &&
                Objects.equals(description, chamber.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exhibit, description);
    }

    @Override
    public Item getExhibit() {
        return exhibit;
    }

    @Override
    public String toString() {
        return "Chamber{" +
                "description='" + description + '\'' +
                '}';
    }
}
