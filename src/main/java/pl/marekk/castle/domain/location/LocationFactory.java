package pl.marekk.castle.domain.location;

import pl.marekk.castle.domain.location.item.AllItemsFactory;
import pl.marekk.castle.domain.location.item.ItemFactory;
import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.annotation.ddd.DomainFactory;

import java.util.concurrent.atomic.AtomicInteger;

@DomainFactory
class LocationFactory {
    private static final String NAME = "room number %s";
    private final ItemFactory itemFactory;
    private final AtomicInteger counter = new AtomicInteger(1);

    private LocationFactory() {
        this(AllItemsFactory.allItemFactory());
    }
    @VisibleForTesting
    LocationFactory(ItemFactory itemFactory) {
        this.itemFactory = itemFactory;
    }
    static LocationFactory locationFactory() {
        return new LocationFactory();
    }

    Location create() {
        return new Chamber(itemFactory.create(), String.format(NAME, counter.incrementAndGet()));
    }
}
