package pl.marekk.castle.domain.location.item;


import pl.marekk.castle.support.IntClosedRange;
import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.random.RealGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static pl.marekk.castle.domain.location.item.BoobyTrapFactory.boobyTrapFactory;
import static pl.marekk.castle.domain.location.item.MonsterFactory.monsterFactory;

public class AllItemsFactory implements ItemFactory<Item> {

    private final List<ItemFactory> factories;
    private final RealGenerator numberGenerator;

    @VisibleForTesting
    AllItemsFactory(List<ItemFactory> factories, RealGenerator numberGenerator) {
        this.factories = factories;
        this.numberGenerator = numberGenerator;
    }

    public static AllItemsFactory allItemFactory() {
        List<ItemFactory> factories = new ArrayList<>();
        Collections.addAll(factories,monsterFactory(), new WeaponFactory(), boobyTrapFactory());
        return new AllItemsFactory(factories, new RealGenerator(new IntClosedRange(0, factories.size()-1)));
    }
    /**
     * creates one boobyTrapFactory all available items in the game
     * @return retrieved item
     */
    @Override
    public Item create() {
        log.debug("using random factory to create an item");
        return factories.get(numberGenerator.generate().get()).create();
    }
}
