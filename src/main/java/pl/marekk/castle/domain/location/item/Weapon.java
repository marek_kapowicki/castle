package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.domain.character.Hero;

import java.util.Random;

import static java.util.Objects.requireNonNull;

enum Weapon implements Item {
    AXE(3), KNIFE(1), SWORD(3);
    private final int points;
    private final String description = "(adds + %s to attack)";

    Weapon(int points) {
        this.points = points;
    }

    @Override
    public Hero applyFor(Hero hero) {
        requireNonNull(hero);
        return hero.withWeapon(this);
    }


    @Override
    public int getPower() {
        return points;
    }

    @Override
    public String toString() {
        return name()+String.format(description, points);
    }
    static Weapon get() {
        int index = new Random().nextInt(values().length);
        return values()[index];
    }

}
