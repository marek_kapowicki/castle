package pl.marekk.castle.domain.location.item;

class WeaponFactory implements ItemFactory<Weapon> {
    @Override
    public Weapon create() {
        log.debug("creating the weapon");
        return Weapon.get();
    }
}
