package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.domain.character.Hero;
import pl.marekk.castle.support.annotation.ddd.ValueObject;

import java.io.Serializable;

/**
 * represents any object that can be discovered
 * during exploration
 */
@ValueObject
public interface Item extends Serializable {
    int getPower();
    Hero applyFor(Hero hero);
    String name();
}
