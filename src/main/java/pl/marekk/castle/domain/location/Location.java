package pl.marekk.castle.domain.location;

import pl.marekk.castle.domain.location.item.Item;

import java.io.Serializable;

@FunctionalInterface
public interface Location extends Serializable {
    Item getExhibit();
}
