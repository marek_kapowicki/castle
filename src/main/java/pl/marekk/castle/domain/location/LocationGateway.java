package pl.marekk.castle.domain.location;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LocationGateway {
    private static final Logger log = LogManager.getLogger(LocationGateway.class);
    private final LocationFactory locationFactory;

    public LocationGateway() {
        this.locationFactory = LocationFactory.locationFactory();
    }

    public LocationGateway(LocationFactory locationFactory) {
        this.locationFactory = locationFactory;
    }

    public Location create() {
        log.debug("creating the location");
        return locationFactory.create();
    }
}
