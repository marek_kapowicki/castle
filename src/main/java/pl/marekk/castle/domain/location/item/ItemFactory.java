package pl.marekk.castle.domain.location.item;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * it is used to create random item boobyTrapFactory given type
 *
 * @param <T> type boobyTrapFactory creating item
 */
@FunctionalInterface
public interface ItemFactory<T extends Item> {
    Logger log = LogManager.getLogger(ItemFactory.class);

    T create();
}
