package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.support.IntClosedRange;
import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.random.RealGenerator;

class BoobyTrapFactory implements ItemFactory<BoobyTrap> {
    private static final IntClosedRange DEFAULT_RANGE = new IntClosedRange(0, 20);
    private final RealGenerator realGenerator;

    @VisibleForTesting
    BoobyTrapFactory(RealGenerator realGenerator) {
        this.realGenerator = realGenerator;
    }

    static BoobyTrapFactory boobyTrapFactory() {
        return new BoobyTrapFactory(new RealGenerator(DEFAULT_RANGE));
    }

    @Override
    public BoobyTrap create() {
        log.debug("creating the boobytrap");
        int power = realGenerator.generate().get();
        return new BoobyTrap(power);
    }
}
