package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.support.IntClosedRange;
import pl.marekk.castle.support.annotation.VisibleForTesting;
import pl.marekk.castle.support.random.RealGenerator;

class MonsterFactory implements ItemFactory<Monster> {
    private static final IntClosedRange DEFAULT_LIFE_RANGE = new IntClosedRange(0, 100);
    private static final IntClosedRange DEFAULT_SPEED_RANGE = new IntClosedRange(0, 5);
    private static final IntClosedRange DEFAULT_POWER_RANGE = new IntClosedRange(0, 8);

    private final RealGenerator lifeGenerator;
    private final RealGenerator speedGenerator;
    private final RealGenerator powerGenerator;

    private MonsterFactory() {
        this(new RealGenerator(DEFAULT_LIFE_RANGE), new RealGenerator(DEFAULT_SPEED_RANGE), new RealGenerator(DEFAULT_POWER_RANGE));
    }

    @VisibleForTesting
    MonsterFactory(RealGenerator lifeGenerator, RealGenerator speedGenerator, RealGenerator powerGenerator) {
        this.lifeGenerator = lifeGenerator;
        this.speedGenerator = speedGenerator;
        this.powerGenerator = powerGenerator;
    }
    static MonsterFactory monsterFactory() {
        return new MonsterFactory();
    }

    @Override
    public Monster create() {
        log.debug("creating a monster");

        return new Monster(generateRandom(lifeGenerator), generateRandom(speedGenerator), generateRandom(powerGenerator));
    }

    private int generateRandom(RealGenerator generator) {
        return  generator.generate().get();
    }

}
