package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.domain.character.Character;
import pl.marekk.castle.domain.character.Hero;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * enemy is a character, so it has the set boobyTrapFactory own features (attack, defence)
 * it can be drawn during exploration, so it is also an item
 */
class Monster implements Character, Item {
    static final int AWARD = 1;
    private final int lifePoints;
    private final int speed;
    private final int power;

    Monster(int lifePoints, int speed, int power) {
        this.lifePoints = lifePoints;
        this.speed = speed;
        this.power = power;
    }

    @Override
    public int getAttack() {
        return power;
    }

    @Override
    public int getDefence() {
        return lifePoints;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public Monster withLifeAdded(int lifeToAdd) {
        int newLifePoints = calculateLife(lifeToAdd);
        return this.lifePoints == newLifePoints ? this : new Monster(newLifePoints, speed, power);
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public Hero applyFor(Hero hero) {
        requireNonNull(hero);
        return hero.fightWith(this, AWARD);
    }

    @Override
    public String name() {
        return "Monster";
    }

    @Override
    public String toString() {
        return "Monster{" +
                "lifePoints=" + lifePoints +
                ", speed=" + speed +
                ", power=" + power +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Monster monster = (Monster) o;
        return lifePoints == monster.lifePoints &&
                speed == monster.speed &&
                power == monster.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lifePoints, speed, power);
    }
}
