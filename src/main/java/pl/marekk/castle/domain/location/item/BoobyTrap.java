package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.domain.character.Hero;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

class BoobyTrap implements Item {
    private final int power;

    BoobyTrap(int power) {
        this.power = power;
    }

    static BoobyTrap of(int power) {
        return new BoobyTrap(power);
    }

    @Override
    public int getPower() {
        return power;
    }

    @Override
    public Hero applyFor(Hero hero) {
        requireNonNull(hero);
        return hero.withLifeAdded(-power);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BoobyTrap boobyTrap = (BoobyTrap) o;
        return power == boobyTrap.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(power);
    }

    @Override
    public String name() {
        return toString();
    }

    @Override
    public String toString() {
        return "BoobyTrap{" +
                "power=" + power +
                " that injures you"+
                '}' ;
    }
}
