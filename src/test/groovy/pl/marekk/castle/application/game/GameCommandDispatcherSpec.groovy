package pl.marekk.castle.application.game

import spock.lang.Specification
import spock.lang.Subject

class GameCommandDispatcherSpec extends Specification {

    Game game = Mock()

    @Subject
    private final GameCommandDispatcher commandDispatcher = new GameCommandDispatcher(game);

    def 'command save verification'() {
        given:
            char letter = 's'
        when:
            commandDispatcher.dispatch(letter)
        then:
            1 * game.save()
    }

    def 'command load verification'() {
        given:
            char letter = 'l'
        when:
            commandDispatcher.dispatch(letter)
        then:
            1 * game.load()
    }

    def 'command explore verification'() {
        given:
            char letter = 'e'
        when:
            commandDispatcher.dispatch(letter)
        then:
            1 * game.explore()
    }

    def 'command help verification'() {
        given:
            char letter = 'h'
        when:
            commandDispatcher.dispatch(letter)
        then:
            1 * game.help()
    }

    def 'command info verification'() {
        given:
            char letter = 'i'
        when:
            commandDispatcher.dispatch(letter)
        then:
            1 * game.status()
    }
}
