package pl.marekk.castle.application.game

import pl.marekk.castle.application.command.CreateHero
import pl.marekk.castle.domain.character.HeroGateway
import pl.marekk.castle.domain.character.HeroRepresentation
import pl.marekk.castle.domain.location.LocationGateway
import pl.marekk.castle.domain.location.Locations
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.location.item.Monsters.HELPLESS

@Narrative("""checks all required stories: 
- hero is created in setup method
- hero explores the room with helpless monster so he wins and gains experience through fighting 
- user can save and load the game
""")
class GameScenarioSpec extends Specification {

    private UUID heroId
    private HeroRepresentation initialStatus
    private HeroGateway heroGateway = new HeroGateway()
    private LocationGateway locationGateway = Mock() {
        create() >> Locations.withItem(HELPLESS)
    }
    @Subject
    private Game game = new Game(heroGateway, locationGateway)

    void setup() {
        heroId = game.init(new CreateHero("marek", "b"))
        initialStatus = game.status()
    }

    def "user explores location and collects experience during fight"() {
        when: 'hero can explore the room'
            game.explore()
        then: 'hero kills the monster inn battle'
            HeroRepresentation statusAfterFight = game.status()
        then: 'hero collects the experience'
            statusAfterFight.experience > initialStatus.experience
    }

    def "user can save and load the game"() {
        given: 'game is saved'
            game.save()
        and: 'game status is changed'
            game.explore()
        when: 'initial game status is load'
            game.load()
        and:
            HeroRepresentation restoredStatus = game.status()
        then: 'restored status is identical as initial'
            restoredStatus == initialStatus


    }
}
