package pl.marekk.castle.application.game

import spock.lang.Specification
import spock.lang.Subject

class MenuDispatcherInteractionSpec extends Specification {

    private GameFlow gameFlow = Mock()

    @Subject
    private Menu menuDispatcher = new Menu(gameFlow)

    def 'command quit verification'() {
        given:
            char letter = 'q'
        when:
            menuDispatcher.dispatch(letter)
        then:
            1 * gameFlow.exit()
    }

    def 'command start verification'() {
        given:
            char letter = 's'
        when:
            menuDispatcher.dispatch(letter)
        then:
            1 * gameFlow.start()
    }

    def 'command load verification'() {
        given:
            char letter = 'l'
        when:
            menuDispatcher.dispatch(letter)
        then:
            1 * gameFlow.load()
    }
}
