package pl.marekk.castle.domain.character

import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.location.item.Items.POWER_TRAP
import static pl.marekk.castle.domain.location.item.Items.TRAP
import static pl.marekk.castle.domain.location.item.Items.WEAPON

class HeroWithItemIteractionSpec extends Specification {

    @Subject
    private Hero hero = Heros.JOE_THIEF

    def 'hero has no items at begging'() {
        expect:
            !hero.weapon
    }

    def 'taking the weapon adds weapon to inventory'() {
        when:
            Hero updated = hero.takeItem(WEAPON)
        then:
            updated.weapon == WEAPON
    }

    def 'taking the trap decreased the defence'() {
        given:
            int lifeAtBeggining = hero.getDefence()
        when:
            Hero updated = hero.takeItem(TRAP)
        then:
           updated.defence == lifeAtBeggining - TRAP.power
    }

    def 'taking the power trap kill the hero'() {

        when:
            Hero updated = hero.takeItem(POWER_TRAP)
        then:
            !updated.alive
    }
}
