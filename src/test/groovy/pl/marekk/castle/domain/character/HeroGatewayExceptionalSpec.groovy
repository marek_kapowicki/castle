package pl.marekk.castle.domain.character

import pl.marekk.castle.application.command.CreateHero
import pl.marekk.castle.application.exception.CastleException
import spock.lang.Specification
import spock.lang.Subject

class HeroGatewayExceptionalSpec extends Specification {
    @Subject
    private final HeroGateway gateway = new HeroGateway();
    private UUID heroId

    void setup() {
        heroId = gateway.createHero(new CreateHero("marek", "BLACKSMITH"))
    }

    def 'throw exception during creation hero from not existing profession'() {
        when:
            gateway.createHero(new CreateHero("marek", 'WRONG'))
        then:
            thrown(CastleException)
    }

    def 'throw exception during retrieving status of not existing hero'() {
        when:
            gateway.getInfo(UUID.randomUUID())
        then:
            thrown(CastleException)
    }

    def 'throw exception during creation hero with unknown #profession'() {
        when:
            gateway.createHero(new CreateHero("marek", profession))
        then:
            thrown(CastleException)
        where:
            profession << ["U", "Unknow", "wrong", "c", ""]
    }
}
