package pl.marekk.castle.domain.character

import pl.marekk.castle.application.exception.CastleException
import spock.lang.Specification
import spock.lang.Subject

class HeroRepositorySpec extends Specification {
    private final Hero stored = Heros.MAREK_BLACKSMITH

    @Subject
    private HeroRepository heroRepository = HeroRepository.of()

    void setup() {
        heroRepository.saveOrUpdate(stored.id, stored)
    }

    def "find existing hero by id"() {
        when:
            Hero retrived = heroRepository.retrieve(stored.id)
        then:
            retrived
    }

    def "throw exception during finding non existing hero by id"() {
        when:
            heroRepository.retrieve(UUID.randomUUID())
        then:
            thrown(CastleException)
    }

    def "update stored hero"() {
        given: 'change the defence'
            Hero newValue = stored.withLifeAdded(1);
        when:
            def updated = heroRepository.saveOrUpdate(stored.id, newValue)
        then:
            updated.defence == stored.defence +1
    }

    def "save not existing hero"() {
        when:
            heroRepository.saveOrUpdate(UUID.randomUUID(), stored.withLifeAdded(1))
        then:
            noExceptionThrown()
    }
}
