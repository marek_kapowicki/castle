package pl.marekk.castle.domain.character

import pl.marekk.castle.domain.character.fight.Battle
import pl.marekk.castle.support.Pair
import spock.lang.Specification

import static pl.marekk.castle.domain.character.Heros.JOE_THIEF
import static pl.marekk.castle.domain.location.item.Monsters.DEAD
import static pl.marekk.castle.domain.location.item.Monsters.GNOME
import static pl.marekk.castle.domain.location.item.Monsters.HELPLESS
import static pl.marekk.castle.domain.location.item.Monsters.IMMORTAL

class HeroFightingSpec extends Specification {

    def 'hero collects experience where win the battle'() {
        given:
            Battle battle = Mock() {
                fight() >> new Pair<Character, Character>(JOE_THIEF, DEAD)
            }
        and:
            int experience = JOE_THIEF.experience
        and:
            int experienceToGain = 3
        when:
            Hero winner = JOE_THIEF.fightWith(GNOME, experienceToGain, battle)
        then:
            winner.experience == experience + experienceToGain
    }

    def 'hero can be killed in battle'() {
        when:
            Hero looser = JOE_THIEF.fightWith(IMMORTAL,3)
        and:
            int experience = JOE_THIEF.experience
        then:
            !looser.alive
        and:
           experience == looser.experience
    }

    def 'hero gains experience after next battle'() {
        given:

            Hero firstBattleWinner = JOE_THIEF.fightWith(HELPLESS, 1)
            int experienceAfterFirstFight = firstBattleWinner.experience
            int experienceToGainInSecondBattle = 1
        when:
            Hero secondBattleWinner = firstBattleWinner.fightWith(HELPLESS, experienceToGainInSecondBattle)
        then:
            secondBattleWinner.experience == experienceAfterFirstFight + experienceToGainInSecondBattle
        
    }
}
