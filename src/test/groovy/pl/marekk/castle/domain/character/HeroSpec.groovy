package pl.marekk.castle.domain.character

import pl.marekk.castle.domain.location.item.Item
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Subject

import static Profession.THIEF
import static pl.marekk.castle.domain.location.item.Items.WEAPON
import static pl.marekk.castle.domain.location.item.Weapon.AXE
import static pl.marekk.castle.domain.location.item.Weapon.SWORD

class HeroSpec extends Specification {
    private static int INIT_LIFE = 100
    private static int INIT_POWER = THIEF.strength

    @Subject
    @Shared
    Hero thief = new Hero("marek", THIEF, INIT_LIFE)

    def 'taking weapon increases the attack'() {
        given:
            Item weapon = SWORD
        when:
            Hero updated = thief.withWeapon(weapon)
        then:
            updated.attack == INIT_POWER + weapon.power
        and:
           updated.weapon == SWORD
    }

    def 'adding second weapon delets previous one'() {
        given:
            Hero withWeapon = thief.withWeapon(SWORD)
        when:
            Hero updated = withWeapon.withWeapon(AXE)
        then:
            updated.weapon == AXE
    }

    def 'changing the defence by adding points=#lifeToAdd'() {

        when:
            Hero updated = thief.withLifeAdded(lifeToAdd as Integer)
        then:
            updated.defence == INIT_LIFE + (lifeToAdd as Integer)
        where:
            lifeToAdd  || expectedDefence
            0          || INIT_LIFE
            2          || INIT_LIFE + 2
            5          || INIT_LIFE + 5
            -2         || INIT_LIFE - 2
            -INIT_LIFE || 0
    }

    def 'killing the here'() {
        given:
            int pointToKill = INIT_LIFE + 2
        when:
            Hero updated = thief.withLifeAdded(-pointToKill)
        then:
            with(updated) {
                !alive
                defence == 0
            }
    }


    def 'updating hero does not change id'() {
        expect:
            thief.id == updated.id
        where:
            updated << [thief.withLifeAdded(1), thief.withWeapon(WEAPON)]
    }
}
