package pl.marekk.castle.domain.character

import pl.marekk.castle.application.command.CreateHero
import pl.marekk.castle.application.command.TakeItem
import pl.marekk.castle.domain.location.item.Item
import pl.marekk.castle.domain.location.item.Items
import spock.lang.Specification
import spock.lang.Subject

class HeroGatewaySpec extends Specification {

    @Subject
    private final HeroGateway gateway = new HeroGateway();
    private UUID heroId
    private HeroRepresentation initialRepresentation

    void setup() {
        heroId = gateway.createHero(new CreateHero("marek", "THIEF"))
        initialRepresentation = gateway.getInfo(heroId)
    }


    def "should retrieve not empty profession' list"() {
        when:
            List<String> professions = gateway.retrieveProfessions()
        then:
            professions
    }

    def 'retrieve status of existing hero'() {
        when:
            HeroRepresentation representation = gateway.getInfo(heroId)
        then:
            representation

    }

    def 'create the hero #profession from command'() {
        when:
            UUID id = gateway.createHero(new CreateHero("marek", profession))
        then:
            id
        where:
            profession << ["T", "Thief", "THIEF", "B", "BLACKSMITH"]
    }

    def 'taking the item changes the hero'() {
        given:
        UUID id = gateway.take(new TakeItem(heroId, (Item) item))
        when:
            HeroRepresentation info = gateway.getInfo(id)
        then:
            info != initialRepresentation
        where:
            item << [Items.TRAP, Items.WEAPON]

    }
}
