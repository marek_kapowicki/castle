package pl.marekk.castle.domain.character

import static Profession.BLACKSMITH
import static Profession.THIEF

class Heros {

    static Hero JOE_THIEF = new HeroFactory().create("joe", THIEF)
    static Hero MAREK_BLACKSMITH = new HeroFactory().create("marek", BLACKSMITH)
}
