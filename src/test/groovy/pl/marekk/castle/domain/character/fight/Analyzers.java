package pl.marekk.castle.domain.character.fight;

import pl.marekk.castle.support.event.EventSuccessAnalyzer;

public class Analyzers {
    public static final EventSuccessAnalyzer ALWAYS_TRUE = t -> true;
    public static final EventSuccessAnalyzer ALWAYS_FALSE = t -> false;
}
