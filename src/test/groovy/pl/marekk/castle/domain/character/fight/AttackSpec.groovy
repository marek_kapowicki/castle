package pl.marekk.castle.domain.character.fight

import pl.marekk.castle.domain.character.Character
import pl.marekk.castle.support.Pair
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.character.Heros.JOE_THIEF
import static pl.marekk.castle.domain.character.fight.Analyzers.ALWAYS_TRUE
import static pl.marekk.castle.domain.character.fight.AttackEvent.of

class AttackSpec extends Specification {

    @Subject
    private AttackEvent succedAttack = of(ALWAYS_TRUE)

    def "hit the enemy"() {
        when:
            Character result = succedAttack.apply(new Pair<Character, Integer>(JOE_THIEF, 5))
        then:
            result.getDefence() == JOE_THIEF.getDefence() - 5

    }

    def "check the success function"() {
        given:
            int initDefence = JOE_THIEF.defence
        when:
            Character result = AttackEvent.hit.apply(new Pair<Character, Integer>(JOE_THIEF, power));
        then:
            result.getDefence() == initDefence - power
        where:
            power << (1..10).collect()

    }

    def "missing does not hurt the defender"() {
        given:

            int initDefence = JOE_THIEF.defence
        when:
            Character result = AttackEvent.miss.apply(new Pair<Character, Integer>(JOE_THIEF, power));
        then:
            result.getDefence() == initDefence
        where:
            power << (1..10).collect()
    }
}
