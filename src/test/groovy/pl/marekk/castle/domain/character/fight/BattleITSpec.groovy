package pl.marekk.castle.domain.character.fight

import pl.marekk.castle.domain.character.Character
import pl.marekk.castle.domain.location.item.Monsters
import pl.marekk.castle.support.Pair
import spock.lang.Specification

import static pl.marekk.castle.domain.character.Heros.JOE_THIEF
import static pl.marekk.castle.domain.character.Heros.MAREK_BLACKSMITH
import static pl.marekk.castle.domain.location.item.Items.WEAPON
import static pl.marekk.castle.domain.location.item.Monsters.ANY
import static pl.marekk.castle.domain.location.item.Monsters.GNOME
import static pl.marekk.castle.domain.location.item.Monsters.HELPLESS

class BattleITSpec extends Specification {
    def "one attender must die #first #second"() {
        when:
            Pair<Character, Character> result = new Battle(GNOME, JOE_THIEF, AttackEvent.of()).fight()
        then:
            !result.left.isAlive() || !result.right.isAlive()
        where:
            first | second
            GNOME | JOE_THIEF
            ANY   | MAREK_BLACKSMITH.withWeapon(WEAPON)
            ANY   | ANY
    }

    def "fighter always kill the helpless enemy"() {
        given:
            Character helpless = HELPLESS
        when:
            Pair<Character, Character> result = new Battle(MAREK_BLACKSMITH, helpless, AttackEvent.of()).fight()
        then:
            !result.right.alive
            result.left.alive
    }

    def "immortal enemy always kills the fighter"() {

        when:
            Pair<Character, Character> result = new Battle(JOE_THIEF, Monsters.IMMORTAL, AttackEvent.of()).fight()
        then:
            result.right.alive
            !result.left.alive
    }
}
