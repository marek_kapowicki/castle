package pl.marekk.castle.domain.character

import spock.lang.Specification
import spock.lang.Subject

import static Profession.BLACKSMITH
import static Profession.THIEF

class HeroFactorySpec extends Specification {
    private static int INITIAL_LIFE = 100
    private static final String MAREK = 'marek'

    @Subject
    HeroFactory heroFactory = new HeroFactory(INITIAL_LIFE);

    def 'create character base on profession = #profession'() {

        when:
            Character hero = heroFactory.create(MAREK, profession);
        then:
            with(hero) {
                attack == profession.strength
                speed == profession.speed
                defence == INITIAL_LIFE
                alive
            }
        where:
            profession << [THIEF, BLACKSMITH]
    }


}
