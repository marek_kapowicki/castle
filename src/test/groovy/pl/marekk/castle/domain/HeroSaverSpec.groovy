package pl.marekk.castle.domain

import pl.marekk.castle.domain.character.Hero
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.character.Heros.JOE_THIEF

class HeroSaverSpec extends Specification {

    private static final String FILE_PATH = "example"
    @Subject
    private final HeroSaver heroSaver = new HeroSaver();

    void setup() {
        heroSaver.save(JOE_THIEF, FILE_PATH)
    }

    def "load hero from file"() {
        when:
            Hero found = heroSaver.load(FILE_PATH)
        then:
            found == JOE_THIEF
    }
}
