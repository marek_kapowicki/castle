package pl.marekk.castle.domain.location.item

import pl.marekk.castle.domain.character.Hero
import spock.lang.Specification

import static pl.marekk.castle.domain.location.item.Monsters.GNOME

class ItemSpec extends Specification {

    Hero hero = Mock()

    def "add weapon to given hero"() {
        given:
            Item weapon = Weapon.KNIFE
        when:
            weapon.applyFor(hero)
        then:
            1 * hero.withWeapon(weapon)
    }

    def "trap injures the hero"() {
        def trapPower = 5
        given:
            Item trap = BoobyTrap.of(trapPower)
        when:
            trap.applyFor(hero)
        then:
            1 * hero.withLifeAdded(-trapPower)
    }

    def "monster fights with hero"() {
        when:
            GNOME.applyFor(hero)
        then:
            1 * hero.fightWith(GNOME, Monster.AWARD)
    }
}
