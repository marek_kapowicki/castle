package pl.marekk.castle.domain.location.item

import pl.marekk.castle.support.random.RealGenerator
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.location.item.Suppliers.*
import static pl.marekk.castle.domain.location.item.Suppliers.POWER_GENERATOR

class MonsterFactorySpec extends Specification {
    RealGenerator generator = Mock() {
        generate() >>> [LIFE_GENERATOR, SPEED_GENERATOR, POWER_GENERATOR]
    }

    @Subject
    private MonsterFactory factory = new MonsterFactory(generator, generator, generator)

    def "create monster with required features"() {
        when:
            Monster enemy = factory.create()
        then:
            with(enemy) {
                defence == LIFE_GENERATOR.get()
                attack == POWER_GENERATOR.get()
                speed == SPEED_GENERATOR.get()
            }

    }
}
