package pl.marekk.castle.domain.location.item

import pl.marekk.castle.support.random.RealGenerator
import spock.lang.Specification
import spock.lang.Subject

import static java.util.Collections.singletonList
import static pl.marekk.castle.domain.location.item.Suppliers.SAMPLE_GENERATOR

class AllItemFactorySpec extends Specification {

    ItemFactory inernal = Mock()
    RealGenerator generator = Mock() {
        generate() >> SAMPLE_GENERATOR
    }
    @Subject
    ItemFactory factory = new AllItemsFactory(singletonList(inernal), generator)

    def "use interlnal factories under the hood"() {
        when:
            factory.create()
        then:
            1 * inernal.create()
    }
}
