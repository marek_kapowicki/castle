package pl.marekk.castle.domain.location

import pl.marekk.castle.domain.location.item.ItemFactory
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.location.item.Suppliers.SAMPLE_FACTORY

class LocationFactorySpec extends Specification {

    private ItemFactory factory = SAMPLE_FACTORY
    @Subject
    private LocationFactory locationFctory = new LocationFactory(factory);

    def "should proxy to internal factory"() {
        when:
            Location location = locationFctory.create()
        then:
            location.exhibit == SAMPLE_FACTORY.create()

    }
}
