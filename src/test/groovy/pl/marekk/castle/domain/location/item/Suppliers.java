package pl.marekk.castle.domain.location.item;

import pl.marekk.castle.support.random.NumberGenerator;

/**
 * groovy class does not recognize java lamda
 * so i use java class
 */
public class Suppliers {
    static final NumberGenerator SAMPLE_GENERATOR = () -> 0;
    static final NumberGenerator POWER_GENERATOR = () -> 10;
    static final NumberGenerator SPEED_GENERATOR = () -> 3;
    static final NumberGenerator LIFE_GENERATOR = () -> 6;
    public static final ItemFactory SAMPLE_FACTORY = () ->  Weapon.SWORD;
}
