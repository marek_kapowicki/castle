package pl.marekk.castle.domain.location

import pl.marekk.castle.domain.location.item.Item

class Locations {
    static Location withItem(Item item) {
        return new Chamber(item, 'sample location')
    }
}
