package pl.marekk.castle.domain.location.item

import pl.marekk.castle.support.random.RealGenerator
import spock.lang.Specification
import spock.lang.Subject

import static pl.marekk.castle.domain.location.item.Suppliers.SAMPLE_GENERATOR

class BoobyTrapFactorySpec extends Specification {

    RealGenerator generator = Mock() {
        generate() >> SAMPLE_GENERATOR
    }
    @Subject
    BoobyTrapFactory factory = new BoobyTrapFactory(generator)

    def 'generate boobytrap with power'() {
        when:
            def boobyTrap = factory.create()
        then:
            boobyTrap.getPower() == SAMPLE_GENERATOR.get()
    }
}
