package pl.marekk.castle.domain.location.item

class Items {
    static final Item WEAPON = Weapon.AXE
    static final Item TRAP = new BoobyTrap(6)
    static final Item POWER_TRAP = new BoobyTrap(Integer.MAX_VALUE)

}
