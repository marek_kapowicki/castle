package pl.marekk.castle.domain.location.item

import pl.marekk.castle.domain.character.Character

class Monsters {
    static final Monster ANY = new MonsterFactory().create()
    static final Monster GNOME = new Monster(10, 3, 2)
    static final Monster HELPLESS = new Monster(10, 3, 0);
    static final Monster DEAD = new Monster(0, 3, 2);
    static final Character IMMORTAL = new Character() {
        @Override
        int getAttack() {
            return 10
        }

        @Override
        int getDefence() {
            return 100
        }

        @Override
        int getSpeed() {
            return 20;
        }

        @Override
        Character withLifeAdded(int life) {
            return this;
        }
    }
}
