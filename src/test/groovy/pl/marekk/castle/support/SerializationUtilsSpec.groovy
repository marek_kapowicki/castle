package pl.marekk.castle.support

import pl.marekk.castle.domain.character.Character
import pl.marekk.castle.domain.location.item.Item
import pl.marekk.castle.domain.location.item.Monsters
import pl.marekk.castle.domain.location.Location
import pl.marekk.castle.domain.location.Locations
import spock.lang.Specification

import static pl.marekk.castle.domain.character.Heros.JOE_THIEF
import static pl.marekk.castle.domain.character.Heros.MAREK_BLACKSMITH
import static pl.marekk.castle.domain.location.item.Items.TRAP
import static pl.marekk.castle.domain.location.item.Items.WEAPON

class SerializationUtilsSpec extends Specification {
    def "serialize and deserialize sample item"() {
        given:
            byte[] bytes = SerializationUtils.serialize(item)
        when:
            Item loaded = SerializationUtils.deserialize(new ByteArrayInputStream(bytes))
        then:
            loaded == item
        where:
            item << [TRAP, WEAPON]
    }

    def "serialize and deserialize sample hero"() {
        given:
            byte[] bytes = SerializationUtils.serialize(character)
        when:
            Character loaded = SerializationUtils.deserialize(new ByteArrayInputStream(bytes))
        then:
            loaded == character
        where:
            character << [JOE_THIEF, MAREK_BLACKSMITH]
    }

    def "serialize and deserialize sample location"() {
        given:
            Location location = Locations.withItem(item)
            byte[] bytes = SerializationUtils.serialize(location);
        when:
            Location loaded = SerializationUtils.deserialize(new ByteArrayInputStream(bytes))
        then:
            loaded == location
        where:
            item << [TRAP, WEAPON]
    }

    def "serialize and deserialize sample monster"() {
            given:
            Item enemy = Monsters.ANY
            byte[] bytes = SerializationUtils.serialize(enemy);
        when:
            Item loaded = SerializationUtils.deserialize(new ByteArrayInputStream(bytes))
        then:
            loaded == enemy
    }

}
